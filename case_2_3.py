'''
Case Study 2.3 - Poor Countries vs Rich Countries
Jake Ullman
'''

from statsmodels.formula.api import ols
import pandas as pd
import statsmodels.formula.api as smf
import statsmodels.api as sm

'''Reading in data'''
dataset = pd.read_csv('poorrich.csv', index_col=0)
# print(dataset)

'''Descriptive statistics'''
print('## DATASET STATISTICS ## ')
print(dataset.describe())

'''Extract the names of control and treatment variables from variable names'''
xnames_df = dataset.iloc[:, 3:]    # names of X variables
xnames_string = '+'.join(xnames_df.columns)
dandxnames_df = dataset.iloc[:, 2:]     # names of D and X variables
dandxnames_string = '+'.join(dandxnames_df.columns)

'''Applying OLS regression as per instruction'''
ols_model = ols("""Outcome ~ """ + dandxnames_string, data=dataset).fit()
ols_model_summary = ols_model.summary()
print(ols_model_summary)

'''Linear regression of y (outcome) on covariates'''
ols_model_y = ols("""Outcome ~  """ + xnames_string , data=dataset).fit_regularized(alpha=0.2, L1_wt=0.5, refit=True)

'''Linear regression of d (treatment) on covariates'''
ols_model_d = smf.ols("""gdpsh465 ~  """ + xnames_string, data=dataset).fit_regularized(alpha=0.2, L1_wt=0.5, refit=True)

'''Apply OLS regression on trained models'''
rY = ols_model_y.resid
rD = ols_model_d.resid
resid_regression = sm.OLS(rY, rD).fit()
resid_regression_summary = resid_regression.summary()
print(resid_regression_summary)

