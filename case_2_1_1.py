'''
Case Study 2.1.1 - Predicting Wages I
Jake Ullman
'''

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import metrics
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import PolynomialFeatures
from sklearn.model_selection import train_test_split

'''Reading in data'''
dataset = pd.read_csv('wage_data.csv')
# print(dataset.head(5))

'''Produce a scatter matrix for each pair of features in the data'''
pd.plotting.scatter_matrix(dataset, alpha = 0.3, figsize = (14,8), diagonal = 'kde')
dataset.hist(figsize = (14,8))
# plt.show()

'''Distribution for the target'''
sns.set(rc={'figure.figsize':(11.7,8.27)})
sns.distplot(dataset['wage'], bins=20)
# plt.show()

'''Adding graphs for correlation, specifically a heat map'''
fig, ax = plt.subplots(figsize=(10,10))
sns.heatmap(dataset.corr(), annot=True, linewidths=.5, ax=ax)
# plt.show()

'''Normalizing Numerical Features'''
scaler = MinMaxScaler()
dataset_scaled = pd.DataFrame(scaler.fit_transform(dataset), index=dataset.index, columns=dataset.columns)
# print(dataset_scaled.shape[1] == dataset_scaled.select_dtypes(include=np.number).shape[1]) # true = numeric
# print(dataset_scaled.head(5))

'''Applying Linear Regression - Getting features and target'''
wages = dataset_scaled['wage']
features = dataset_scaled.drop('wage', axis = 1)

'''Quadratic - polynomial features'''
polynomial_features = PolynomialFeatures(degree=2)
features_poly = polynomial_features.fit_transform(features)
# print(features.head(5))
# print(features_poly)

'''Split the 'features' and 'wage' data into training and testing sets'''
X_train, X_test, y_train, y_test = train_test_split(features, wages, test_size = 0.2, random_state = 0)

'''Split the 'features' and 'wage' - polynomial version'''
X_train_poly, X_test_poly, y_train_poly, y_test_poly = train_test_split(features_poly, wages, test_size = 0.2, random_state = 0)

'''Normal - Training linearly regression + predicting'''
reg = LinearRegression()
reg.fit(X_train, y_train)
y_pred = reg.predict(X_test)

'''Polynomial - Quadratic - Training linear regression + predicting'''
reg_poly = LinearRegression()
reg_poly.fit(X_train_poly, y_train_poly)
y_pred_poly = reg_poly.predict(X_test_poly)

'''Looking at coefficients, normal and polynomial trains'''
coeff_df = pd.DataFrame(reg.coef_, features.columns, columns=['Coefficient'])
# print(coeff_df)
df = pd.DataFrame({'Actual': y_test, 'Predicted': y_pred})
# print(df.head(20))
df_poly = pd.DataFrame({'Actual': y_test_poly, 'Predicted': y_pred_poly})

'''Visualizing data with bar graphs'''
df.head(20).plot(kind='bar',figsize=(10,8))
plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
plt.show()
df_poly.head(20).plot(kind='bar',figsize=(10,8))
plt.grid(which='major', linestyle='-', linewidth='0.5', color='green')
plt.grid(which='minor', linestyle=':', linewidth='0.5', color='black')
plt.show()

'''Quantifying results of regression'''
print ('Normal Regression')
print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred))
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred))
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred)))
score = reg.score(X_test, y_test)
print("Score: ", score)
print ('Quadratic Regression')
print('Mean Absolute Error:', metrics.mean_absolute_error(y_test, y_pred_poly))
print('Mean Squared Error:', metrics.mean_squared_error(y_test, y_pred_poly))
print('Root Mean Squared Error:', np.sqrt(metrics.mean_squared_error(y_test, y_pred_poly)))
score_poly = reg_poly.score(X_test_poly,y_test_poly)
print("Score: ",score_poly)
