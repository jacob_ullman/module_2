'''
Case Study 2.4 - Predicting Wages II
Jake Ullman
'''

import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import PolynomialFeatures
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LinearRegression, Lasso, Ridge, LassoLars
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import cross_validate
from sklearn.pipeline import Pipeline
from google.colab import files

'''Loading in data'''
data = pd.read_csv('/content/drive/MyDrive/Colab Notebooks/wage_data_2.csv', index_col=0)
data_features = data.drop(['wage', 'lwage'],  axis=1)
print(data_features.head(5))
print(data.describe())

'''Preprocessing using min max'''
scaler = MinMaxScaler()
data_features_scaled = pd.DataFrame(scaler.fit_transform(data_features), index=data_features.index, columns=data_features.columns)
print(data_features_scaled.head(5))

'''Getting features and target'''
wages = LabelEncoder().fit_transform(data['lwage'])
features = data_features_scaled

'''Polynomial features'''
polynomial_features = PolynomialFeatures(degree=2)
features_poly = polynomial_features.fit_transform(features)
print(features.head(5))

'''Split the 'features' and 'wage' data into training and testing sets'''
X_train, X_test, y_train, y_test = train_test_split(features, wages, test_size = 0.2, random_state = 0)

'''Split the 'features' and 'wage' data for polynomial'''
X_train_poly, X_test_poly, y_train_poly, y_test_poly = train_test_split(features_poly, wages, test_size = 0.2, random_state = 0)

'''Performing an analysis of different algorithms'''
classifiers = {
    'LinearRegression': LinearRegression(),
    'LinearRegressionPoly': LinearRegression(),
    'Lasso': Lasso(alpha=1),
    'LassoLars': LassoLars(),
    'Ridge': Ridge(alpha=0),
    # 'ElasticNet': ElasticNet(alpha=.5),
    'RandomForest': RandomForestClassifier(n_estimators=100, max_depth=6, min_samples_leaf=5),
    # 'BoostingTree': GradientBoostingClassifier(n_estimators=1000, max_depth=2),
    'MLPClassifier': MLPClassifier(hidden_layer_sizes=(5,), max_iter=100)
}
results = {}

for classifier in classifiers:
    pipe = Pipeline(steps=[('classifier', classifiers[classifier])])
    results[classifier] = {}
    if (classifier == 'LinearRegressionPoly'):
        pipe.fit(X_train_poly, y_train_poly)
        y_train_pred_poly = pipe.predict(X_train_poly)
        y_test_pred_poly = pipe.predict(X_test_poly)
        results[classifier]['model score'] = pipe.score(X_test_poly, y_test_poly)
        results[classifier]['mean square'] = cross_validate(pipe, X_train_poly, y_train_poly)['test_score'].mean()
        results[classifier]['Train RMSE'] = mean_squared_error(y_train_poly, y_train_pred_poly, squared=False)
        results[classifier]['Test RMSE'] = mean_squared_error(y_test_poly, y_test_pred_poly, squared=False)

    else:
        pipe.fit(X_train, y_train)
        y_train_pred = pipe.predict(X_train)
        y_test_pred = pipe.predict(X_test)
        results[classifier]['model score'] = pipe.score(X_test, y_test)
        results[classifier]['mean square'] = cross_validate(pipe, X_train, y_train)['test_score'].mean()
        results[classifier]['Train RMSE'] = mean_squared_error(y_train, y_train_pred, squared=False)
        results[classifier]['Test RMSE'] = mean_squared_error(y_test, y_test_pred, squared=False)


df_results = pd.DataFrame.from_dict(results, orient='index').sort_values(['model score'], ascending=False)
print(df_results)